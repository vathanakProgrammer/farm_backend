package back.end

import grails.converters.JSON

class StudentController {

    def index() {
        def student = new Student()
        render(student as  JSON)
    }

    def show(){
        def student = Student.list()
        render(student as JSON)
    }
}
