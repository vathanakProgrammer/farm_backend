package back.end

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
        "/api/student/list"(controller: "Student",action: "index", method:"GET")
        "/api/student/show"(controller: "Student",action: "show", method:"GET")
    }
}
